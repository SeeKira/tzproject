import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderComponent } from "./pages/slider/slider.component";
import { EventInfoModalComponent } from "./components/event-info-modal/event-info-modal.component";
import { EventsRoutingModule } from "./events-routing.module";
import { CarouselModule } from 'ngx-owl-carousel-o';
import { DatePipe } from "@angular/common";
import { MatDialogModule } from "@angular/material/dialog";
import { MatIconModule } from "@angular/material/icon";

@NgModule({
  declarations: [SliderComponent, EventInfoModalComponent],
  imports: [
    CommonModule,
    EventsRoutingModule,
    CarouselModule,
    MatDialogModule,
    MatIconModule
  ],
  providers: [DatePipe]
})
export class EventsModule { }

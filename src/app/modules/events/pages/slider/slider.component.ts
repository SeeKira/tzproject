import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../../core/services/auth.service";
import {EventsService} from "../../../../core/services/events.service";
import {OwlOptions} from "ngx-owl-carousel-o";
import {environment} from "../../../../../environments/environment";
import {DatePipe} from "@angular/common";
import {MatDialog} from "@angular/material/dialog";
import {EventInfoModalComponent} from "../../components/event-info-modal/event-info-modal.component";
import {EventModel, EventsModel} from "../../../../core/models/event.model";

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  url = environment.IMG_ENDPOINT;
  events: EventsModel[] = [];

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    dots: false,
    navSpeed: 600,
    navText: ['', ''],
    items: 1,
    merge: true,
    autoWidth: true,
    autoplay: true,
    autoplaySpeed: 1000
  }

  constructor(
    private authService: AuthService,
    private eventsService: EventsService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.authService.loginTest().subscribe((res: any) => {
      const token = res.headers.get('x-auth-token');
      localStorage.setItem('token', token);
      this.getEvents();
    });
  }

  private getEvents() {
    this.eventsService.getEvents().subscribe((res) => {
      this.events = res;
    });
  }

  public showEventInfo(slide: EventsModel) {
    const dialogRef = this.dialog.open(EventInfoModalComponent, {
      width: '40%',
      data: slide.events[0]
    });
  }

}

import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {EventModel} from "../../../../core/models/event.model";

@Component({
  selector: 'app-event-info-modal',
  templateUrl: './event-info-modal.component.html',
  styleUrls: ['./event-info-modal.component.scss']
})
export class EventInfoModalComponent {

  constructor(
    public dialogRef: MatDialogRef<EventInfoModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EventModel
  ) {
  }

  public close() {
    this.dialogRef.close();
  }
}

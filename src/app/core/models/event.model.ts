export class EventsModel {
  date: string;
  events: EventModel[]

  constructor(date: string, events: EventModel[]){
    this.date = date;
    this.events = events;
  }
}

export class EventModel {
  NearestSchedule: any;
  name: any;
  poster: any;
  short_description: any;

  constructor(NearestSchedule: any, name: any, poster: any, short_description: any){
    this.NearestSchedule = NearestSchedule;
    this.name = name;
    this.poster = poster;
    this.short_description = short_description;
  }
}

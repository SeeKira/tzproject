import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import {Observable} from "rxjs";
import {EventsModel} from "../models/event.model";

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(
    private apiService: ApiService
  ) { }

  public getEvents(): Observable<EventsModel[]>  {
    const request = `nearest_events_by_date?date=2021-09-09&date_interval=90`;
    return this.apiService.get(request);
  }

}

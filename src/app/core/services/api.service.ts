import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ApiService {

  private host = environment.API_ENDPOINT;

  constructor(
    private http: HttpClient,
  ) {
  }

  public get(route: any, params?: any): Observable<any> {
    return this.http.get(`${this.host}/${route}`, params);
  }

  public post(route: any, data?: any, params?: any): Observable<any> {
    return this.http.post(`${this.host}/${route}`, data, params);
  }

  public put(route: any, data: any, params?: any): Observable<any> {
    return this.http.put(`${this.host}/${route}`, data, params);
  }

  public delete(route: any, param?: any): Observable<any> {
    return this.http.delete(`${this.host}/${route}/${param}`);
  }


}

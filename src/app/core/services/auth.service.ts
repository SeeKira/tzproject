import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private apiService: ApiService,
    private http: HttpClient
  ) { }

  loginTest(): Observable<any> {
    const header = {'Content-Type':'application/json',};
    return this.apiService.get('/settings', {
      headers: header,
      observe: 'response',
      responseType: 'json'
    });
  }

}

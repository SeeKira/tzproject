import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventsModule } from "./modules/events/events.module";

const routes: Routes = [
  { path: '', loadChildren: () => import('./modules/events/events.module').then(m => m.EventsModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

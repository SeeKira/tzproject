export const environment = {
  production: true,
  API_ENDPOINT: 'https://zapomni.lastick.ru/api/widget/v1',
  IMG_ENDPOINT: 'https://zapomni.lastick.ru/'
};
